from collections import defaultdict
# lines that begin with $ are commands you executed
# cd means change directory
# cd x moves in one level
# cd .. moves out one level
# cd / switches the current directory to the outermost directory
# ls means list
# 123 abc means that the current directory contains a file named abc with size 123
# dir xyz means that the current directory contains a directory named xyz
# total size of each directory
TOTAL_DISK_SPACE = 70000000
NEED_MIN = 30000000
unusedSpace = 0 #totalDiskSpace - needMin
spaceNeeded = 0
chemin = []
directories = defaultdict(int)
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
    lines = lines.strip().replace("\n", "")
    if "$ ls" in lines or "dir" in lines:
        continue
    if ".." in lines:
        chemin.pop()
    elif "$ cd" in lines:
        chemin += [lines.split()[2]]
    else:
        for i in range(1,len(chemin)+1):
            directories["/".join(chemin[:i])] += int(lines.split()[0])
partOne = 0
for values in directories.values():
    if int(values) <= 100000:
        partOne += values

candidats = []
unusedSpace = TOTAL_DISK_SPACE - int(directories["/"])
spaceNeeded = NEED_MIN - unusedSpace

for keys in directories.keys():
    if directories[keys] >= spaceNeeded:
        candidats.append([keys,directories[keys]])

candidats.sort(key = lambda candidats: candidats[1])
print("Part 1 : " + str(partOne))
print("Part 2 : " + str(candidats[0][1]))
