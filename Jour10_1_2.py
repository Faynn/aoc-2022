import copy

SignalsStrength = {
    20: 0,
    60: 0,
    100: 0,
    140: 0,
    180: 0,
    220: 0
}
cycles = 0
x = 1
P1 = 0
P2 = [['.' for i in range(40)] for j in range(6)]
row = 0
col = 0
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
    i = 0
    if cycles in SignalsStrength.keys() and SignalsStrength[cycles] == 0:
        SignalsStrength[cycles] = cycles * x

    if lines.startswith("noop"):
        cycles += 1
        if col + 1 == x or col == x or col - 1 == x:
            P2[row][col] = "#"
        col += 1
    else:
        while i < 2:
            if col >= 40:
                row += 1
                col = 0
            if col + 1 == x or col == x or col - 1 == x:
                P2[row][col] = "#"
                col += 1
            else:
                col += 1
                if col >= 40:
                    row += 1
                    col = 0
            cycles += 1
            if cycles in SignalsStrength.keys() and SignalsStrength[cycles] == 0:
                SignalsStrength[cycles] = cycles * x
            i += 1
        if i == 2:
            x += int(lines.split()[1])


for k, v in SignalsStrength.items():
    P1 += v
print("Part 1 : " + str(P1))
print("Part 2 : ")
for rows in P2:
    print(rows)
