import copy
from copy import deepcopy
def checkClose(h,t):
    a,b = h[0], h[1]
    x,y = t[0], t[1]

    # left
    if x == a and y - 1 == b or a == x and b - 1 == y:
        return True
    # Up left
    if x + 1 == a and y - 1 == b or a + 1 == x and b - 1 == y:
        return True
    # Up
    if x + 1 == a and y == b or a + 1 == x and b == y:
        return True
    # Up right
    if x + 1 == a and y + 1 == b or a + 1 == x and b + 1 == y:
        return True
    # right
    if x == a and y + 1 == b or a == x and b + 1 == y:
        return True
    # down right
    if x - 1 == a and y + 1 == b or a - 1 == x and b + 1 == y:
        return True
    # down
    if x - 1 == a and y == b or a - 1 == x and b == y:
        return True

    # overlap
    if a == x and b == y:
        return True
    return False

H = [0, 0]
T = [0, 0]
coordsT = []
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
    direction, steps = lines.strip().split()

    for i in range(int(steps)):
        Z = copy.copy(H)
        if direction == "U":
            H[0] += 1
        elif direction == "D":
            H[0] -= 1
        elif direction == "R":
            H[1] += 1
        elif direction == "L":
            H[1] -= 1
        if not checkClose(H, T):  # T ne bouge pas
            T = Z # Prend l'ancienne position de H
        if T not in coordsT:
            coordsT.append(T)

print("Part 1 : " + str(len(coordsT)))
