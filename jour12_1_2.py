import collections

def jour12(part):
    heightmap = []
    row = 0
    Q = collections.deque()

    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
        strRow = lines.replace("\n", "")
        if "S" in strRow:
            S_position = (row, strRow.find("S"))
            Q.append(((row, strRow.find("S")), 0))
        if part == 2:
            if "a" in strRow:
                while 'a' in strRow:
                    Q.append(((row, strRow.find("a")), 0))
                    strRow = strRow[:strRow.find('a')] + strRow[strRow.find("a")].replace('a', '#') + strRow[strRow.find('a')+1:]
        strRow = strRow.replace("#","a")
        heightmap += [list(map(int, [ord(x) for x in strRow]))]
        row += 1

    paths = []
    visited = set()
    while Q:
        cell = Q.popleft()
        row, col, steps = cell[0][0], cell[0][1], cell[1]

        if heightmap[row][col] == ord('E'):
            paths.append(steps)

        if (row, col) in visited:
            continue
        visited.add((row, col))

        directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]  # Gauche, Droite, Haut, Bas

        for direction in directions:
            newRow = row + direction[0]
            newCol = col + direction[1]

            if 0 <= newRow < len(heightmap) and 0 <= newCol < len(heightmap[0]):
                if (newRow, newCol) not in visited:
                    if part == 1 and heightmap[row][col] == ord('S'):
                        Q.append(((newRow, newCol), steps + 1))
                    else:
                        if 0 <= newRow < len(heightmap) and 0 <= newCol < len(heightmap[0]) and heightmap[newRow][
                            newCol] <= heightmap[row][col] + 1:
                            Q.append(((newRow, newCol), steps + 1))
    print(paths)


print(jour12(1))
print(jour12(2))
