lowercases = "abcdefghijklmnopqrstuvwxyz"
uppercases = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

ptsPartUn = 0
ptsPartDeux = 0
cpt = 0
group = []
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
	ruckStackOne,ruckStackTwo = lines[:len(lines) // 2],lines[len(lines) // 2:]
	partUn = set([x for x in [y for y in ruckStackOne if y in ruckStackTwo]])
	for car in partUn:
		if car.isupper():
			ptsPartUn += uppercases.index(car) + 27
		else:
			ptsPartUn += lowercases.find(car) + 1
	group.append(lines.replace("\n",""))
	cpt += 1
	if cpt == 3:
		cpt = 0
		stackOne, stackTwo, stackThree = group[0], group[1],group[2]
		partDeux = set([x for x in [y for y in [z for z in stackThree] if y in stackOne] if x in stackTwo])
		for car in partDeux:
			if car.isupper():
				ptsPartDeux += uppercases.index(car) + 27
			else:
				ptsPartDeux += lowercases.find(car) + 1
		group = []

print(ptsPartUn)
print(ptsPartDeux)
