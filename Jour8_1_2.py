grid = []
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
	row = list(map(int,lines.replace("\n","")))
	grid += [row]

def checkVoisins(tree,grid,i,j):
	row = i
	col = j
	flagUp = False
	flagRight = False
	flagDown = False
	flagLeft = False
	xUp = 0
	xRight = 0
	xDown = 0
	xLeft = 0
	# UP
	while row >= 0:
		row -= 1
		if row >= 0:
			if grid[row][col] >= tree:
				flagUp = True
				xUp += 1
				break
			else:
				xUp += 1
	row = i
	col = j
	# RIGHT
	while col < len(grid[row]):
		col += 1
		if col < len(grid[row]):
			if grid[row][col] >= tree:
				flagRight = True
				xRight += 1
				break
			else:
				xRight += 1

	row = i
	col = j
	# DOWN
	while row < len(grid):
		row += 1
		if row < len(grid):
			if grid[row][col] >= tree:
				flagDown = True
				xDown += 1
				break
			else:
				xDown += 1

	row = i
	col = j
	# LEFT
	while col >= 0:
		col -= 1
		if col >= 0:
			if grid[row][col] >= tree:
				flagLeft = True
				xLeft += 1
				break
			else:
				xLeft += 1
	return (xUp * xDown * xLeft * xRight,True if not flagLeft or not flagDown or not flagRight or not flagUp else False)
    
partOne = 0
temp = []
for i in range(len(grid)):
	for j in range(len(grid[i])):
		tree = grid[i][j]
		if checkVoisins(tree,grid,i,j)[1]:
			partOne += 1
		temp.append(checkVoisins(tree,grid,i,j)[0])
print("Part 1 : " + str(partOne))
print("Part 2 : " + str(max(temp)))
