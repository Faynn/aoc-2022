import math
from collections import defaultdict
import copy
from copy import deepcopy
P1 = 20
P2Divisions = []
S = defaultdict(list)
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
	lines = lines.replace("\n","").strip()
	if lines != "\n":
		# Monkey
		if lines.startswith("Monkey"):
			monkeyNumber = int(lines.replace(":","").split(" ")[1])
			S[monkeyNumber] = []
		# Items
		if "Starting" in lines:
			if "," in lines:
				S[monkeyNumber] = [list(map(int,lines[lines.find(":")+2:].split(", ")))]
			else:
				S[monkeyNumber] = [[int(lines[lines.find(":")+2:])]]
		# Opération du singe
		if "Operation" in lines:
			if lines[lines.find(":")+2:].split(" ")[-1] == "old":
				S[monkeyNumber].append([lines[lines.find(":")+2:].split(" ")[-2],lines[lines.find(":")+2:].split(" ")[-1]])
			else:
				S[monkeyNumber].append([lines[lines.find(":") + 2:].split(" ")[-2], int(lines[lines.find(":") + 2:].split(" ")[-1])])

		if "Test" in lines:
			divPar = int(lines[lines.find("by")+2:])
			S[monkeyNumber].append([divPar])
			P2Divisions.append(divPar)

		if "If" in lines:
			if "true" in lines:
				S[monkeyNumber].append([int(lines[lines.find(":") + 1:].split(" ")[-1])])
			else:
				S[monkeyNumber][3].insert(0,int(lines[lines.find(":") + 1:].split(" ")[-1]))
				S[monkeyNumber].append([0])

S2 = copy.deepcopy(S)
round = 0
monkeyNumber = 0
worryLvl = 0
while round < P1:
	while S[monkeyNumber][0]:
		S[monkeyNumber][-1][0] += 1
		worryLvl = S[monkeyNumber][0][0]
		worryLvlCopy = copy.copy(worryLvl)
		if S[monkeyNumber][1][0] == "*":
			if S[monkeyNumber][1][1] == "old":
				worryLvl *= worryLvlCopy
			else:
				worryLvl *= S[monkeyNumber][1][1]
		elif S[monkeyNumber][1][0] == "+":
			if S[monkeyNumber][1][1] == "old":
				worryLvl += worryLvlCopy
			else:
				worryLvl += S[monkeyNumber][1][1]
		worryLvl = math.floor(worryLvl/3)
		# Vérifier à quel singe on doit remettre l'item
		if worryLvl % S[monkeyNumber][2][0]: # True
			S[S[monkeyNumber][3][0]][0] += [worryLvl]
			S[monkeyNumber][0].pop(0)
		else: # False
			S[S[monkeyNumber][3][1]][0] += [worryLvl]
			S[monkeyNumber][0].pop(0)
	if monkeyNumber == 7:
		round += 1
		monkeyNumber = 0
	else:
		monkeyNumber += 1
totalInspections = []
for keys,values in S.items():
	totalInspections += [values[4]]
print("Part 1 : " + str(sorted(totalInspections)[::-1][0][0] * sorted(totalInspections)[::-1][1][0]))

# Aller chercher le nombre qui permettra de mieux manage notre worry level
# On prend chacun des diviseurs et on va mettre à jour le multipleCommun avec le multiple le plus bas qui est divisible par le nouveau nombre passé
# https://math.libretexts.org/Bookshelves/PreAlgebra/Book%3A_Prealgebra_(OpenStax)/02%3A_Introduction_to_the_Language_of_Algebra/2.10%3A_Prime_Factorization_and_the_Least_Common_Multiple_(Part_2)
# Ex. : 47, 5 & 36. -> 47 -> 47 * (47 * 5) = 11045, 11045 * (11045 * 36) = 4391712900. 4391712900 % 5 = 0, 4391712900 % 47 = 0 et 4391712900 % 36 = 0
multipleCommun = 1
for divs in P2Divisions:
	multipleCommun *= math.lcm(multipleCommun,divs)
P2 = 10000
round = 0
while round < P2:
	while S2[monkeyNumber][0]:
		S2[monkeyNumber][-1][0] += 1
		worryLvl = S2[monkeyNumber][0][0]
		worryLvlCopy = copy.copy(worryLvl)
		if S2[monkeyNumber][1][0] == "*":
			if S2[monkeyNumber][1][1] == "old":
				worryLvl *= worryLvlCopy
			else:
				worryLvl *= S2[monkeyNumber][1][1]
		elif S2[monkeyNumber][1][0] == "+":
			if S2[monkeyNumber][1][1] == "old":
				worryLvl += worryLvlCopy
			else:
				worryLvl += S2[monkeyNumber][1][1]
		worryLvl %= multipleCommun
		# Vérifier à quel singe on doit remettre l'item
		if worryLvl % S2[monkeyNumber][2][0]: # True
			S2[S2[monkeyNumber][3][0]][0] += [worryLvl]
			S2[monkeyNumber][0].pop(0)
		else: # False
			S2[S2[monkeyNumber][3][1]][0] += [worryLvl]
			S2[monkeyNumber][0].pop(0)
	if monkeyNumber == 7:
		round += 1
		monkeyNumber = 0
	else:
		monkeyNumber += 1

totalInspections = []
for keys,values in S2.items():
	totalInspections += [values[4]]
print("Part 2 : " + str(sorted(totalInspections)[::-1][0][0] * sorted(totalInspections)[::-1][1][0]))
