for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
	i = 0
	j = 1
	seen = []
	count = 0
	flag = True
	while True:
		seen.append(lines[i])
		j = i + 1
		while j < len(lines):
			if len(seen) == 14: # Remplacer '14' par '4' pour Part 1
				count += j
				flag = False
				break
			if lines[j] not in seen:
				seen.append(lines[j])
				j += 1
			else:
				seen = []
				break
		i += 1
		if not flag:
			break

print("Part 1 & 2 answer :" + str(count))
