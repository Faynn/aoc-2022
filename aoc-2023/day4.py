# Day 4 (2023) Part 1 & Part 2
import re
import numpy
part1 = 0
part2 = 0

tableP1 = []
tableP2 = []

for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
    tableP1.append(lines)
    tableP2.append(lines)

# Part 1
for scratchcards in tableP1:
    scratchcardNumber = scratchcards[scratchcards.find(" "):scratchcards.find(":")].strip()

    lines = scratchcards[scratchcards.find(":")+1:].replace("\n","").strip()

    winningNumbers, myNumbers = lines.strip().split("|")
    myNumbers = myNumbers.strip().split(" ")
    winningNumbers = winningNumbers.strip().split(" ")

    double = False
    inc = 0
    for number in myNumbers:
        if number != "" and number in winningNumbers and not double:
            inc += 1
            double = True
        elif number != "" and number in winningNumbers and double:
            inc = (inc * 2)
        else:
            continue
    part1 += inc


# Part 2
for scratchcards in tableP2:
    scratchcardNumber = scratchcards[scratchcards.find(" "):scratchcards.find(":")].strip()

    lines = scratchcards[scratchcards.find(":")+1:].replace("\n","").strip()

    winningNumbers, myNumbers = lines.strip().split("|")
    myNumbers = myNumbers.strip().split(" ")
    winningNumbers = winningNumbers.strip().split(" ")

    newScratchcards = 0
    for number in myNumbers:
        if number != "" and number in winningNumbers:
            newScratchcards += 1
        else:
            continue

    # Ajout des nouvelles scratchcards
    for i in range(newScratchcards):
        if i + int(scratchcardNumber) < len(tableP1):
            tableP2.append(tableP1[int(scratchcardNumber) + i])

print('Part 1 : ', part1)
print('Part 2 : ', len(tableP2))
