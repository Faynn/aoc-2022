from itertools import permutations
import copy
def GetDistance(galaxyA,galaxyB):
    return abs(galaxyA[0] - galaxyB[0]) + abs(galaxyA[1] - galaxyB[1])


def FindOptimalPath(galaxiesLocations):
    total = 0
    while galaxiesLocations:
        # Remove the one we're looking at
        galaxyA = galaxiesLocations.pop(0)

        # Check every possible pairs with the chosen Galaxy and keep the shortest distance between A and B
        for i in range(len(galaxiesLocations)):
            steps = GetDistance(galaxyA,galaxiesLocations[i])
            total += steps

    return total

def GetGalaxiesLocations(extandedImage):
    galaxiesLocations = []

    for i in range(len(extandedImage)):
        for j in range(len(extandedImage[i])):
            if extandedImage[i][j] == '#': # A galaxy
                galaxiesLocations.append((i,j))

    return galaxiesLocations

def ExpandImage(image,isPart2,galaxiesLocations = None):
    expandedImage = []
    addCols = []
    addRows = []
    col = 0
    if not isPart2:
        while col < len(image[0]):
            validCol = True
            row = 0
            while row < len(image):
                if image[row][col] == '#':
                    validCol = False
                    break

                row += 1
            col += 1

            if validCol:
                addCols.append(col-1)

        # Managing the rows
        for rows in image:
            # Adding the extra dots depending on the columns
            checked = 0
            for dotsInd in addCols:
                rows = rows[: dotsInd + checked] + '.' + rows[dotsInd + checked:]
                checked += 1
            if '#' not in rows:
                expandedImage.append(rows)
                expandedImage.append(rows)
            else:
                expandedImage.append(rows)
    else:
        while col < len(image[0]):
            validCol = True
            row = 0
            while row < len(image):
                if image[row][col] == '#':
                    validCol = False
                    break

                row += 1
            col += 1

            if validCol:
                addCols.append(col - 1)


        indRow = 0
        while indRow < len(image):
            if '#' not in image[indRow]:
                addRows.append(indRow)
            indRow += 1
        print(addRows)
        print(addCols)
        print(galaxiesLocations)
        # Managing the rows

        # Adding the extra dots depending on the columns

        for locations in galaxiesLocations:
            # Check X
            x = locations[0]
            y = locations[1]
            for newRows in addRows:
                if locations[0] > newRows:
                    x += 999999
            # Check Y
            for newCols in addCols:
                if locations[1] > newCols:
                    y += 999999

            # replace the old location with the new location
            galaxiesLocations.remove(locations)
            galaxiesLocations.insert(0,(x,y))

    return expandedImage

def PartOne():
    image = []
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        # Check if galaxy in image, if not, expend it 3 times
        image.append(lines.replace('\n',''))

    galaxiesLocations = GetGalaxiesLocations(extandedImage=ExpandImage(image,False))

    return FindOptimalPath(galaxiesLocations)

def PartTwo():
    image = []
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        # Check if galaxy in image, if not, expend it 3 times
        image.append(lines.replace('\n', ''))

    galaxiesLocations = GetGalaxiesLocations(image)
    extandedImage = ExpandImage(image, True,galaxiesLocations)

    return FindOptimalPath(galaxiesLocations)

print('Part 1 : ', PartOne())
print('Part 2 : ', PartTwo())