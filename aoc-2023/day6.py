# Day 6 (2023) Part 1 & Part 2
import re

def PartOne():
    cpt = 0
    part1 = 1
    races = []
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        if 'Time:' in lines:
            while re.search(r"\d",lines):
                time = re.search(r"\d+",lines)[0]
                lines = lines[lines.find(time) + len(time):]
                races.append([int(time)])
        else:
            while re.search(r"\d",lines):
                distance = re.search(r"\d+",lines)[0]
                races[cpt].append(int(distance))
                lines = lines[lines.find(distance) + len(distance):]
                cpt += 1

    waysToBeat = []

    for race in races:
        temp = 0
        # count how many different ways we can beat that record for that race
        maxTime = race[0]
        maxDistance = race[1]
        currentTime = 0
        distancePerMiliseconds = 0
        while currentTime < maxTime:
            currentTime += 1
            distancePerMiliseconds += 1

            # check how far we can get
            if (maxTime - currentTime) * distancePerMiliseconds > maxDistance:
                temp += 1
        waysToBeat.append(temp)

    for vals in waysToBeat:
        part1 *= vals

    return part1


def PartTwo():
    cpt = 0
    part2 = 0
    races = []
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        if 'Time:' in lines:
            while re.search(r"\d", lines):
                time = re.search(r"\d+", lines)[0]
                lines = lines[lines.find(time) + len(time):]
                races.append([int(time)])
        else:
            while re.search(r"\d", lines):
                distance = re.search(r"\d+", lines)[0]
                races[cpt].append(int(distance))
                lines = lines[lines.find(distance) + len(distance):]
                cpt += 1

    realTime = ''
    realDistance = ''
    for x in races:
        realTime += str(x[0])
        realDistance += str(x[1])

    races = [[int(realTime),int(realDistance)]]

    for race in races:
        maxTime = race[0]
        maxDistance = race[1]
        currentTime = 0
        distancePerMiliseconds = 0

        while currentTime < maxTime:
            currentTime += 1
            distancePerMiliseconds += 1

            if (maxTime - currentTime) * distancePerMiliseconds > maxDistance:
                part2 += 1

    return part2

print('Part 1 : ',PartOne())
print('Part 2 : ',PartTwo())
