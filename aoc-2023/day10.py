import copy

# one large, continuous loop
def GetConnections(start,sketch,updatedSketch, visited):
    pipes = {
        '|': [[-1, 0], [1, 0]],  # Up and down
        '-': [[0, 1], [0, -1]],  # Right and left
        'L': [[-1, 0], [0, 1]],  # Up and right
        'J': [[-1, 0], [0, -1]],  # Up and left
        '7': [[1, 0], [0, -1]],  # down and left
        'F': [[1, 0], [0, 1]],  # down and right
    }
    toCheck = []
    x,y,steps = start[0],start[1],start[2]

    if sketch[x][y] == 'S':
        directions = [(1, 0), (-1, 0), (0, 1), (0, -1)]  # All neighbors
    else:
        directions = pipes[sketch[x][y]]
    for direction in directions:
        tempX = direction[0] + x
        tempY = direction[1] + y

        if sketch[tempX][tempY] != '0' and sketch[tempX][tempY] != 'S' and 0 <= tempX < len(sketch) and 0 <= tempY < len(sketch[0]):
            # if we went right
            if x == tempX and tempY > y:
                # Does that pipe connect to the left?
                if [0,-1] in pipes[sketch[tempX][tempY]] and [tempX,tempY] not in visited:
                    updatedSketch[tempX][tempY] = str(steps + 1)
                    toCheck.append([tempX,tempY,steps + 1])
            # if we went left
            elif x == tempX and tempY < y:
                # Does that pipe connect to the right?
                if [0,1] in pipes[sketch[tempX][tempY]] and [tempX,tempY] not in visited:
                    updatedSketch[tempX][tempY] = str(steps + 1)
                    toCheck.append([tempX, tempY, steps + 1])
            # if we went down
            elif x < tempX and tempY == y:
                # Does that pipe connect to the top?
                if [-1,0] in pipes[sketch[tempX][tempY]] and [tempX,tempY] not in visited:
                    updatedSketch[tempX][tempY] = str(steps + 1)
                    toCheck.append([tempX, tempY, steps + 1])
            # if we went up
            elif x > tempX and tempY == y:
                # Does that pipe connect to the bottom?
                if [1,0] in pipes[sketch[tempX][tempY]] and [tempX,tempY] not in visited:
                    updatedSketch[tempX][tempY] = str(steps + 1)
                    toCheck.append([tempX, tempY, steps + 1])
    return toCheck


def GetPath(start,updatedSketch,sketch, visited):
    stack = [start]

    while stack:
        current = stack.pop()
        stack += GetConnections(current,sketch,updatedSketch,visited)
        visited.append([current[0],current[1]])

    maximum = -1
    for rows in updatedSketch:
        temp = ['|','-','L','J','7','F','S']
        for vals in temp:
            while vals in rows:
                indRows = rows.index(vals)
                rows[indRows] = 0

        if max(map(int,rows)) > maximum:
            maximum = max(map(int,rows))
    return round(maximum // 2) + 1



def PartOne():
    global startX, startY
    sketch = []
    updatedSketch = []
    row = 0
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        lines = lines.replace('.','0')
        # Starting position
        if 'S' in lines:
            col = lines.index('S')
            startX = row
            startY = col
        sketch.append([x for x in lines.strip()])

        row += 1
    updatedSketch = copy.deepcopy(sketch)
    return GetPath([startX,startY,0],updatedSketch,sketch, [])

print('Part 1 : ',PartOne())