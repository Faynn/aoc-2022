# Day 1 (2023) Part 1 & Part 2
import re

totalP1 = 0
totalP2 = 0

dictio = {
	"one" : 1,
	"two" : 2,
	"three" : 3,
	"four" : 4,
	"five" : 5,
	"six" : 6,
	"seven" : 7,
	"eight" : 8,
	"nine" : 9
}

for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
	# Part 2
	temp = []

	for keys in dictio:
		if keys in lines:
			indexLetters = lines.find(keys)
			indexLetters2 = lines.rfind(keys)
			temp.append([keys,indexLetters])
			temp.append([keys,indexLetters2])

	if re.search(r"\d",lines):
		firstNumber = re.search(r"\d",lines)[0]
		indexFirstNumber = lines.find(firstNumber)

		if re.search(r"\d",lines):
			secondNumber = re.search(r"\d",lines[::-1])[0]
			indexSecondNumber = lines.rfind(secondNumber)
			temp.append([secondNumber,indexSecondNumber])


		temp.append([firstNumber,indexFirstNumber])
		
	temp.sort(key = lambda x: x[1])

	try:
		FirstNumber = int(str(temp[0][0]))
	except:
		FirstNumber = dictio[str(temp[0][0])]

	try:
		SecondNumber = int(str(temp[-1][0]))
	except:
		SecondNumber = dictio[str(temp[-1][0])]

	addingUp = str(FirstNumber) + str(SecondNumber)

	totalP2 += int(addingUp)

	# Part 1
	if re.search(r"\d",lines):
		firstNumber = re.search(r"\d",lines)[0] # Needed for part 1
		secondNumber = re.search(r"\d",lines[::-1])[0]
		totalNumbers = str(firstNumber[0]) + str(secondNumber[0])
		totalP1 += int(totalNumbers)
	else:
		totalNumbers = str(firstNumber[0]) + str(firstNumber[0])
		totalP1 += int(totalNumbers)

print("Part 1 : ", totalP1)
print("Part 2 : ", totalP2)



