# Day 12 (2023) Part 1
import itertools
import copy

def CountKnownSprings(springRow):
    return springRow.count('#')

def CountMissingSprings(knownCount,totalSprings):
    return totalSprings - knownCount

def GetIndexesOfUnknownSprings(springRow):
    return [ind for ind,val in enumerate(springRow) if val == '?']


def IsAValidArrangement(stringRowCopy,springBlocks):
    return [len(x) for x in stringRowCopy.replace('?','.').split('.') if x != ''] == [x for x in springBlocks]


def TryDifferentArrangements(springRow,missingSprings,sizes):
    possibleArrangements = 0

    ind = GetIndexesOfUnknownSprings(springRow)

    lstPossibleAdditions = list(itertools.combinations(ind,missingSprings))

    for possibleAdditions in lstPossibleAdditions:
        stringRowCopy = copy.deepcopy(springRow)
        for i in range(len(possibleAdditions)):
            stringRowCopy = stringRowCopy[:possibleAdditions[i]] + '#' + stringRowCopy[possibleAdditions[i]+1:]

        # Check if valid arrangement
        if IsAValidArrangement(stringRowCopy,sizes):
            possibleArrangements += 1
    return possibleArrangements

def PartOne():
    possibleArrangements = 0
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        springRow, sizes = lines.replace('\n','').split(' ')
        sizes = list(map(int,sizes.split(',')))

        knownSpringsAmount = CountKnownSprings(springRow)
        missingSprings = CountMissingSprings(knownSpringsAmount,sum(sizes))

        possibleArrangements += TryDifferentArrangements(springRow,missingSprings,sizes)
    return possibleArrangements

print('Part 1 : ',PartOne())