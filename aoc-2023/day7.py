# Day 7 (2023) Part 1 & Part 2
def EvaluateHands(hands, isPartTwo):
    if not isPartTwo:
        # [['KK677', '28'], ['KTJJT', '220']]
        levels = {
            'A': '14',
            'K': '13',
            'Q': '12',
            'J': '11',
            'T': '10',
            '9': '09',
            '8': '08',
            '7': '07',
            '6': '06',
            '5': '05',
            '4': '04',
            '3': '03',
            '2': '02',
        }
        rankings = []

        for hand in hands:
            value = ''
            for i in range(len(hand[0])):
                value += levels[hand[0][i]]
            rankings.append([value,hand[1]])
        return sorted(rankings, key=lambda x: x[0])
    else:
        # [['KK677', '28'], ['KTJJT', '220']]
        levels = {
            'A': '13',
            'K': '12',
            'Q': '11',
            'T': '10',
            '9': '09',
            '8': '08',
            '7': '07',
            '6': '06',
            '5': '05',
            '4': '04',
            '3': '03',
            '2': '02',
            'J': '01',
        }
        rankings = []
        for hand in hands:
            value = ''
            for i in range(len(hand[0])):
                value += levels[hand[0][i]]
            rankings.append([value, hand[1]])

        return sorted(rankings, key=lambda x: x[0])


def GetType(hand, isPartTwo):
    types = {
        'Five of a kind': [5,1,7],
        'Four of a kind': [4,2,6],
        'Full house': [3,2,5],
        'Three of a kind': [3,3,4], # 3 pareilles mais 3 types de cartes en tout -> TTT98
        'Two pair': [2,3,3],
        'One pair': [2,4,2],
        'High card': [1,5,1]
    }

    if not isPartTwo:
        temp = {occurence: hand.count(occurence) for occurence in set(hand)}
        amountOfDiffLabels = len(temp)
        maximum = -1
        for keys,values in temp.items():
            if values > maximum:
                maximum = values
        numberOfPairs = maximum

        return [i for i in types if types[i][:2] == [numberOfPairs,amountOfDiffLabels]][0]
    else:
        couldBecome = ['A','K','Q','T','J','9','8','7','6','5','4','3','2']
        bestType = ''
        HighestType = 0
        for i in range(len(couldBecome)):
            newHand = hand.replace('J',couldBecome[i])

            temp = {occurence: newHand.count(occurence) for occurence in set(newHand)}
            amountOfDiffLabels = len(temp)

            maximum = -1
            for keys, values in temp.items():
                if values > maximum:
                    maximum = values
            numberOfPairs = maximum

            for keys,values in types.items():
                if types[keys][:2] == [numberOfPairs,amountOfDiffLabels] and types[keys][2] > HighestType:
                    HighestType = types[keys][2]
                    bestType = keys

        return bestType

def PartOne():
    totalWinnings = 0
    types = {
        'High card': [],
        'One pair': [],
        'Two pair': [],
        'Three of a kind': [],  # 3 pareilles mais 3 types de cartes en tout -> TTT98
        'Full house': [],
        'Four of a kind': [],
        'Five of a kind': [],
    }
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        hand,bid = lines.replace('\n','').split(" ")
        typeOfHand = GetType(hand, False)
        types[typeOfHand].append([hand,bid])

    rank = 1
    for types,hands in types.items(): # reverse it. Start from lowest value to highest
        if len(hands) == 1:
            totalWinnings += int(hands[0][1]) * rank
            rank += 1
        else:
            order = EvaluateHands(hands, False)
            for i in range(len(order)):
                totalWinnings += int(order[i][1]) * rank
                rank += 1
    return totalWinnings

def PartTwo():
    totalWinnings = 0
    types = {
        'High card': [],
        'One pair': [],
        'Two pair': [],
        'Three of a kind': [],  # 3 pareilles mais 3 types de cartes en tout -> TTT98
        'Full house': [],
        'Four of a kind': [],
        'Five of a kind': [],
    }
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        hand, bid = lines.replace('\n', '').split(" ")

        # [JKKK2, KKKK2]
        typeOfHand = GetType(hand,True)
        types[typeOfHand].append([hand, bid])
    rank = 1
    for types, hands in types.items():  # reverse it. Start from lowest value to highest
        if len(hands) == 1:
            totalWinnings += int(hands[0][1]) * rank
            rank += 1
        else:
            order = EvaluateHands(hands, True)
            for i in range(len(order)):
                totalWinnings += int(order[i][1]) * rank
                rank += 1
    return totalWinnings

print('Part 1 : ',PartOne())
print('Part 2 : ',PartTwo())
