# Day 2 (2023) Part 1 & Part 2

sumIds = 0
power = 0

# only 12 red cubes, 13 green cubes, and 14 blue cubes
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
	possible = True
	minBlue = 0
	minGreen = 0
	minRed = 0

	gameId = lines[lines.find('Game ')+5:lines.find(':')]

	lines = lines[lines.find(":")+1:]
	sets = lines.strip().split(";")
	
	for i in range(len(sets)):
		sets[i] = sets[i].split(',')


	for i in range(len(sets)):

		dictio = {
		'blue' : 14,
		'green': 13,
		'red' : 12

		}
		for j in range(len(sets[i])):
			lookingAt = sets[i][j].strip().split(' ')
			dictio[lookingAt[1]] -= int(lookingAt[0])


			if lookingAt[1] == "blue" and int(lookingAt[0]) > minBlue:
				minBlue = int(lookingAt[0])

			if lookingAt[1] == "green" and int(lookingAt[0]) > minGreen:
				minGreen = int(lookingAt[0])

			if lookingAt[1] == "red" and int(lookingAt[0]) > minRed:
				minRed = int(lookingAt[0])

			

		for keys,values in dictio.items():
			if values < 0:
				possible = False
				break

	power += (minBlue * minGreen * minRed)

	if possible:
		sumIds += int(gameId)




print(sumIds)
print(power)
