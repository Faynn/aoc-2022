# Day 3 (2023) Part 1 & Part 2
import re
import numpy
col = 0
board = []

ignore = ".0123456789"

part1Output = 0
part2Output = 0

# only 12 red cubes, 13 green cubes, and 14 blue cubes
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
	board.append(lines.replace("\n",""))

part2Dictio = {
}

for i in range(len(board)):
	for j in range(len(board[i])):
		
		col = j

		if board[i][j] in ignore[1:]:
			number = re.search(r"\d+",board[i])[0]
			length = len(number)


			directions = [(1,0),(-1,0),(0,1),(0,-1),(1,1),(1,-1),(-1,1),(-1,-1)]
			
			completed = False
			for k in range(length):

				for dirs in directions:
					# Check for a symbol
					if 0 <= i + dirs[0] < len(board) and 0 <= col + dirs[1] < len(board[i]):
						if board[i + dirs[0]][col + dirs[1]] not in ignore:
							part1Output += int(number)
							completed = True

							# (1,3) = [467,35]
							if (i + dirs[0],col + dirs[1]) not in part2Dictio:
								part2Dictio[(i + dirs[0],col + dirs[1])] = [int(number)]
							else:
								part2Dictio[(i + dirs[0], col + dirs[1])].append(int(number))


							break
				col += 1
				if completed:
					break

			ind = board[i].find(number)

			for a in range(length):
				updatedRow = board[i][:ind+a] + '.' + board[i][ind+a + 1:]
				board[i] = updatedRow

for keys,values in part2Dictio.items():
	if len(values) > 1:
		part2Output += numpy.prod(values)


print('Part 1 : ', part1Output)
print('Part 2 : ', part2Output)
