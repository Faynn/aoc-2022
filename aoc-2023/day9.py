# day 9 (2023) PArt 1 & Part 2

def GetSumOfExtrapolatedValues(histories, isPartTwo):
    totalSum = 0
    if not isPartTwo:
        for i in range(len(histories)):
            totalSum += histories[i][-1]
    else:
        for i in range(len(histories)):
            totalSum += histories[i][0]
    return totalSum

def GetDifference(x,y):
    diff = y - x
    return diff

def GetPreviousValues(histories):
    histories = histories[::-1]  # Bottom up
    # add a 0 at the beginning
    histories[0].insert(0,0)

    for i in range(1, len(histories)):
        decreasedBy = histories[i - 1][0]
        numberToBeDecreased = histories[i][0]
        newNumber = numberToBeDecreased - decreasedBy
        histories[i].insert(0,newNumber) # Adding the new number at the beginning
    return histories


def GetNextValues(histories):
    histories = histories[::-1] # Bottom up
    # add a 0 at the end
    histories[0].append(0)

    for i in range(1,len(histories)):
        increaseBy = histories[i-1][-1]
        numberToBeIncreased = histories[i][-1]
        newNumber = increaseBy + numberToBeIncreased
        histories[i].append(newNumber)
    return histories

def Predict(history,y,isPartTwo):
    # 0 3 6 9 12 15
    x = [history]

    while sum(x[-1]) != 0:
        newHistory = []
        for i in range(len(x[-1])-1):
            diff = GetDifference(x[-1][i], x[-1][i+1])
            newHistory.append(diff)
        x.append(newHistory)
    if not isPartTwo:
        x = GetNextValues(x)
    else:
        x = GetPreviousValues(x)

    y.append(x[::-1][0])
    return y


def PartOne():
    y = []
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
        # [10,13,16,21,30,45]
        history = list(map(int,lines.strip().replace('\n','').split(' ')))

        y = Predict(history,y, False)

    return GetSumOfExtrapolatedValues(y,False)

def PartTwo():
    y = []
    for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):

        history = list(map(int, lines.strip().replace('\n', '').split(' ')))

        y = Predict(history, y, True)

    return GetSumOfExtrapolatedValues(y,True)

print('Part 1 : ',PartOne())
print('Part 2 : ',PartTwo())
