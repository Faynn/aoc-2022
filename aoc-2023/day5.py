# Day 5 (2023) Part 1 & Part 2
seeds = []
dictio = {}
def CategorizePartOne(input_file):
    key = ""
    with open(input_file, 'r') as file:
        input_text = file.read()

    for categories in input_text.strip().split('\n\n'):
        if 'seeds' in categories:
            seeds = categories.replace("\n", "")[categories.find(":") + 1:].strip().split(" ")
            dictio['seeds'] = seeds
        else:

            categorie = categories.strip().split("\n")
            gotKey = False

            for items in categorie:
                if gotKey:
                    dictio[key] += [items]
                else:
                    key = items
                    dictio[items] = []
                    gotKey = True

def Mapping(type_of_map,seed, isPartTwo):
    xy = {}

    if not isPartTwo:
        #Find min and Max with the starting values we have for a specific seed
        for i in range(len(dictio[type_of_map])):
            a,b,c = dictio[type_of_map][i].split(" ")
            xy[b,'->', str(int(b) + int(c) - 1)] = a
        for keys, values in xy.items():
            a = int(keys[0])
            b = int(keys[2])
            c = int(values)
            if seed >= a and seed <= b: # Compris dans le range
                if seed >= a:
                    return (seed - a) + c
                elif seed <= b:
                    return (b - seed) + c
        return seed
    else:
        # Find min and Max with the starting values we have for a specific seed
        for i in range(len(dictio[type_of_map])):
            b, a, c = dictio[type_of_map][i].split(" ")
            xy[b, '->', str(int(b) + int(c) - 1)] = a
        for keys, values in xy.items():
            a = int(keys[0])
            b = int(keys[2])
            c = int(values)
            if seed >= a and seed <= b:  # Compris dans le range
                if seed >= a:
                    return (seed - a) + c
                elif seed <= b:
                    return (b - seed) + c
        return seed

liste = ['seed-to-soil map:','soil-to-fertilizer map:','fertilizer-to-water map:','water-to-light map:','light-to-temperature map:','temperature-to-humidity map:','humidity-to-location map:']

def PartOne():
    CategorizePartOne("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt")
    finalDestination = []
    for seed in dictio['seeds']:
        seed = int(seed)

        for items in liste:
            seed = Mapping(items,seed,False)
        finalDestination.append(seed)
    return finalDestination


def GetSeedRangesPartTwo():
    a = 0
    b = 1
    dictioP2 = {}
    while a < len(dictio['seeds']) and b < len(dictio['seeds']):
        dictioP2[int(dictio['seeds'][a])] = int(dictio['seeds'][a]) + int(dictio['seeds'][b])
        a += 2
        b += 2
    return dictioP2


def IsValid(seed,dict):
    for key,values in dict.items():
        if key <= seed and values >= seed:
            return True
    return False

def PartTwo():
    CategorizePartOne("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt")
    dictioP2 = GetSeedRangesPartTwo()

    # We'll start by checking if we can find a valid seed that has a destination of 0
    destination = 0
    listeP2 = liste[::-1] # Reversing the list of categories. We'll start from the end and make our way to the top
    seed = 0
    while True:
        firstItterationDone = False
        for categories in listeP2:
            # First, starting with distance 0. We'll get a list of different seeds that have a destination of 0
            if not firstItterationDone:
                seed = Mapping(categories,destination,True)
                firstItterationDone = True
            else:
                seed = Mapping(categories, seed,True)

        if IsValid(seed,dictioP2): # Si valide, on trouvé la destination la plus proche
            return destination
        else:
            destination += 1

print('Part 1 ',min(PartOne()))
print('Part 2 ',PartTwo())
