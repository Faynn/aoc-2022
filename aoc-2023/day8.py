# Day 8 (2023) Part 1 & Part 2
import math
networkNodes = {}

def FollowInstructions(instructions,networkNodes,position):
	steps = 0
	L = 0
	R = 1

	if position == 'AAA':
		while True:
			for i in range(len(instructions)):
				# Update position
				if instructions[i] == 'L':
					position = networkNodes[position][L]
				else:
					position = networkNodes[position][R]
				steps += 1
				if position == 'ZZZ':
					return steps
	else:
		i = 0
		a = []
		while True:
			k = 0
			while k < len(position):
				if position[k][-1] == 'Z':
					a.append(steps)
					position.pop(k)
				k += 1
			if not position:
				return math.lcm(*a)
			if i >= len(instructions):
				i = 0
			instruction = instructions[i]
			cpt = 0
			for positions in position:
				# Update position
				if instruction == 'L':
					pos = networkNodes[positions][L]
				else:
					pos = networkNodes[positions][R]
				position[cpt] = pos
				cpt += 1
			i += 1
			steps += 1

def PartOne():
	gotInstructions = False
	instructions = ''
	for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
		if lines == '\n':
			gotInstructions = True
		elif not gotInstructions:
			instructions += lines.strip().replace('\n','')
		else:
			source,connections = lines.split(' = ')
			a,b = connections.strip().replace('(','').replace(')','').split(', ')
			networkNodes[source] = [a,b]
	return FollowInstructions(instructions,networkNodes,'AAA')

def GetStartingNodes(networkNodes):
	validStartingNodes = []
	for keys in networkNodes.keys():
		if keys[-1] == 'A':
			validStartingNodes.append(keys)
	return validStartingNodes

def PartTwo():
	gotInstructions = False
	instructions = ''
	for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aoc.txt"):
		if lines == '\n':
			gotInstructions = True
		elif not gotInstructions:
			instructions += lines.strip().replace('\n','')
		else:
			source,connections = lines.split(' = ')
			a,b = connections.strip().replace('(','').replace(')','').split(', ')
			networkNodes[source] = [a,b]
	return FollowInstructions(instructions,networkNodes,GetStartingNodes(networkNodes))



print('Part 1 : ',PartOne())
print('Part 2 : ',PartTwo())