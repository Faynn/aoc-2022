#[ ] [ ] [G] [ ] [ ] [D] [ ] [Q] [ ]
#[P] [ ] [T] [ ] [ ] [L] [M] [Z] [ ]
#[Z] [Z] [C] [ ] [ ] [Z] [G] [W] [ ]
#[M] [B] [F] [ ] [ ] [P] [C] [H] [N]
#[T] [S] [R] [ ] [H] [W] [R] [L] [W]
#[R] [T] [Q] [Z] [R] [S] [Z] [F] [P]
#[C] [N] [H] [R] [N] [H] [D] [J] [Q]
#[N] [D] [M] [G] [Z] [F] [W] [S] [S]
# 1   2   3   4   5   6   7   8   9

cratesP1 = [[],[],[],[],[],[],[],[],[]]
cratesP2 = [[],[],[],[],[],[],[],[],[]]

# Part 1
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
	if lines.startswith("move"):
		a,b,c = map(int,lines.replace("move","").replace("from","").replace("to","").split())
		while a > 0:
			if cratesP1[b-1] != []:
				cratesP1[c-1].insert(0,cratesP1[b-1].pop(0))
			a-=1

	elif lines.find("[") != -1:
		col = 0
		for i in range(len(lines)):
			if lines[i] == "[" and lines[i+1] == " ":
				col += 1
			elif lines[i] == "[" and lines[i+1] != " ":
				cratesP1[col].append(lines[i+1])
				col += 1
			else:
				continue

# Part 2
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
	if lines.startswith("move"):
		a,b,c = map(int,lines.replace("move","").replace("from","").replace("to","").split())
		if a > 1:
			boxes = cratesP2[b - 1][:a][::-1]
			while boxes:
				box = boxes[0]
				cratesP2[c - 1].insert(0, box)
				cratesP2[b-1].pop(0)
				boxes.pop(0)
		else:
			if len(cratesP2[b - 1]) > 0:
				cratesP2[c - 1].insert(0, cratesP2[b - 1].pop(0))

	elif lines.find("[") != -1:
		col = 0
		for i in range(len(lines)):
			if lines[i] == "[" and lines[i+1] == " ":
				col += 1
			elif lines[i] == "[" and lines[i+1] != " ":
				cratesP2[col].append(lines[i+1])
				col += 1
			else:
				continue

outputP2 = ""
outputP1 = ""

for i in range(len(cratesP1)):
	if cratesP1[i] != []:
		outputP1 += cratesP1[i][0]
for i in range(len(cratesP2)):
	if cratesP2[i] != []:
		outputP2 += cratesP2[i][0]
		
print("Part 1 : " + outputP1)
print("Part 2 : " + outputP2)
