def partOneSolution(puzzleInput):
	countP1 = 0
	for lines in open(puzzleInput):
		pairUn, pairDeux = lines.split(",")
		pairUn = pairUn.split("-")
		pairDeux = pairDeux.split("-")

		if int(pairUn[0]) <= int(pairDeux[0]) and int(pairUn[1]) >= int(pairDeux[1]):
			countP1 += 1
		elif int(pairDeux[0]) <= int(pairUn[0]) and int(pairDeux[1]) >= int(pairUn[1]):
			countP1 += 1
	return countP1

def partTwoSolution(puzzleInput):
	countP2 = 0
	for lines in open(puzzleInput):
		pairUn, pairDeux = lines.split(",")
		pairUn = pairUn.split("-")
		pairDeux = pairDeux.split("-")
		lsPairUn = []
		lsPairDeux = []
		i = int(pairUn[0])
		j = int(pairDeux[0])
		while i <= int(pairUn[1]):
			lsPairUn.append(i)
			i += 1
		while j <= int(pairDeux[1]):
			lsPairDeux.append(j)
			j += 1

		if len([x for x in [y for y in lsPairDeux] if x in lsPairUn]) > 0:
			countP2 += 1
	return countP2

print(partOneSolution("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"))
print(partTwoSolution("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"))

##################################
#  Cleaner solution avec pandas: #
##################################
import pandas as panda
def partOneSolution(s1,e1,s2,e2):
	if s1 <= s2 and e1 >= e2:
		return 1
	elif s2 <= s1 and e2 >= e1:
		return 1
	return 0

def partTwoSolution(s1,e1,s2,e2):
	intervalUn = panda.Interval(s1,e1,closed='both')
	intervalDeux = panda.Interval(s2,e2,closed='both')
	if intervalUn.overlaps(intervalDeux) or intervalDeux.overlaps(intervalUn):
		return 1
	return 0

countP1 = 0
countP2 = 0
for lines in open("C:\\Users\\tikev\\OneDrive\\Bureau\\aocInput.txt"):
	pairUn, pairDeux = lines.split(",")[0].split("-"),lines.split(",")[1].split("-")
	s1 = int(pairUn[0])
	e1 = int(pairUn[1])
	s2 = int(pairDeux[0])
	e2 = int(pairDeux[1])
	countP1 += partOneSolution(s1,e1,s2,e2)
	countP2 += partTwoSolution(s1,e1,s2,e2)
print(countP1)
print(countP2)
